# First import the library
import pyrealsense2.pyrealsense2 as rs
import numpy as np


class RealStream:
    pipeline = None
    console_debug = False
    depth_scale = 0

    def __init__(self, console_debug=False):
        self.console_debug = console_debug
        # Create a context object. This object owns the handles to all connected realsense devices
        self.pipeline = rs.pipeline()
        self.pipeline.start()
        self.depth_scale = self.pipeline.get_active_profile().get_device().first_depth_sensor().get_depth_scale()

    def print_to_console(self, depth):
        # Print a simple text-based representation of the image, by breaking it into 10x20 pixel regions and approximating the coverage of pixels within one meter
        coverage = [0] * 64
        for y in range(480):
            for x in range(640):
                dist = depth.get_distance(x, y)
                if 0 < dist < 1:
                    coverage[int(x / 10)] += 1

            if y % 20 == 19:
                line = ""
                for c in coverage:
                    line += " .:nhBXWW"[int(c / 25)]
                coverage = [0] * 64
                print(line)

    def run(self, function):
        try:
            while True:
                # Create a pipeline object. This object configures the streaming camera and owns it's handle
                frames = self.pipeline.wait_for_frames()
                depth = frames.get_depth_frame()
                if not depth: continue
                if self.console_debug:
                    self.print_to_console(depth)

                depth_data = depth.as_frame().get_data()
                np_image = np.asanyarray(depth_data)
                function(self.depth_scale * np_image)   # depth converted to meters
        finally:
            self.pipeline.stop()

    def __del__(self):
        if self.pipeline is not None:
            self.pipeline.stop()
