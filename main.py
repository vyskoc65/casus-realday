from real_stream import *
import numpy as np
import matplotlib.pyplot as plt
import asyncio
import websockets
import json

DEPTH_CUT = 6
SEGMENTS_ROWS = 3
SEGMENTS_COLS = 5
RESOLUTION_ROWS = 480
RESOLUTION_COLS = 640

DEBUG=False

step_rows = RESOLUTION_ROWS // SEGMENTS_ROWS
step_cols = RESOLUTION_COLS // SEGMENTS_COLS

storage = np.empty(shape=(SEGMENTS_ROWS, SEGMENTS_COLS, 0), dtype=float)
classPrefix = "graphics.scenery.websocketserver."

volume_uuid = None

async def send_message(direction):
    if volume_uuid is None:
        return
    uri = "ws://0.0.0.0:8765/ws"
    async with websockets.connect(uri) as websocket:
        message = {"command": "move",
                   "uuid": volume_uuid,
                   "payload": {"type": classPrefix + "ExchangeVectorComponent",
                               "component": 0,
                               "value": direction * 0.01,
                               "mode": "ADD"
                               }
                   }
        message_json = json.dumps(message)
        await websocket.send(message_json)
        response = await websocket.recv()
        print(f"<<< {response}")

async def send_rotation(speed):
    if volume_uuid is None:
        return
    uri = "ws://0.0.0.0:8765/ws"
    async with websockets.connect(uri) as websocket:
        message = {"command": "rotationSpeed",
                   "uuid": volume_uuid,
                   "payload": {"type": classPrefix + "ExchangeFloat",
                               "value": speed
                               }
                   }
        message_json = json.dumps(message)
        await websocket.send(message_json)
        response = await websocket.recv()
        if DEBUG:
            print(f"<<< {response}")

async def send_tint(tint):
    if volume_uuid is None:
        return
    uri = "ws://0.0.0.0:8765/ws"
    async with websockets.connect(uri) as websocket:
        message = {"command": "colormapTint",
                   "uuid": volume_uuid,
                   "payload": {"type": classPrefix + "ExchangeFloat",
                               "value": tint
                               }
                   }
        message_json = json.dumps(message)
        await websocket.send(message_json)
        response = await websocket.recv()
        if DEBUG:
            print(f"<<< {response}")

async def get_volume_uuid():
    global volume_uuid
    uri = "ws://0.0.0.0:8765/ws"
    async with websockets.connect(uri) as websocket:
        await websocket.send("getAllNodes")
        nodes_raw = await websocket.recv()
        nodes_message = json.loads(nodes_raw)
        nodes = nodes_message["nodes"]
        for node in nodes:
            if node["name"] == "volume":
                volume_uuid = node["uuid"]
        print(volume_uuid)


def peek(data):
    global storage
    indices_rows = range(0, RESOLUTION_ROWS, step_rows)
    indices_cols = range(0, RESOLUTION_COLS, step_cols)
    blobiness = np.add.reduceat(np.add.reduceat(data, indices_rows, 0), indices_cols, 1)
    blobiness = blobiness / (step_cols * step_rows)
    storage = np.append(storage, np.atleast_3d(blobiness), axis=2)
    rotation_speed = (np.sum(blobiness[:, 0])-10) * 0.0003
    rotation_speed = np.clip(rotation_speed, -0.002, 0.002)
    asyncio.run(send_rotation(rotation_speed))
    tint = np.sum(blobiness[:, 2]) * 0.1
    asyncio.run(send_tint(tint))
    if DEBUG:
        print(np.sum(blobiness[:, 0]), np.sum(blobiness[:, 4]), np.sum(blobiness[:, 2]), rotation_speed, tint)


if __name__ == '__main__':
    try:
        asyncio.run(get_volume_uuid())
        stream = RealStream()
        stream.run(peek)

    finally:
        print(storage.shape)
        np.save("storage.npy", storage)
